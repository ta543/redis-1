import React from 'react';
import AnalyticsDisplay from './components/AnalyticsDisplay';
import StreamInput from './components/StreamInput';
import './App.css';

function App() {
    return (
        <div className="App">
            <h1>Real-Time Analytics Dashboard</h1>
            <StreamInput />
            <AnalyticsDisplay />
        </div>
    );
}

export default App;
