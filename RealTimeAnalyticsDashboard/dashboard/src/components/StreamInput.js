import React, { useState } from 'react';

function StreamInput() {
    const [streamData, setStreamData] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        await fetch('/streams/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ streamKey: 'testStream', data: { message: streamData } }),
        });
        setStreamData('');
    };

    return (
        <form onSubmit={handleSubmit}>
            <label>
                Enter Data:
                <input type="text" value={streamData} onChange={(e) => setStreamData(e.target.value)} />
            </label>
            <button type="submit">Send</button>
        </form>
    );
}

export default StreamInput;
