import React, { useEffect, useState } from 'react';

function AnalyticsDisplay() {
    const [analyticsData, setAnalyticsData] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch('/analytics/data');
            const data = await response.json();
            setAnalyticsData(data.messages);
        };

        fetchData();
    }, []);

    return (
        <div>
            <h2>Analytics Data</h2>
            <ul>
                {analyticsData.map((data, index) => (
                    <li key={index}>{JSON.stringify(data)}</li>
                ))}
            </ul>
        </div>
    );
}

export default AnalyticsDisplay;
