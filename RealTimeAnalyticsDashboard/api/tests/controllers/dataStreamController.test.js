const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../app'); // Adjust this path based on your structure
const should = chai.should();

chai.use(chaiHttp);

describe('DataStream Controller', () => {
    it('should add data to the Redis stream on /streams/add POST', done => {
        chai.request(server)
            .post('/streams/add')
            .send({ streamKey: 'testStream', data: { field1: 'value1', field2: 'value2' } })
            .end((err, res) => {
                res.should.have.status(201);
                res.body.should.be.a('object');
                res.body.should.have.property('message').eql('Data added to stream');
                done();
            });
    });
});
