const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../app'); // Adjust this path based on your structure
const should = chai.should();

chai.use(chaiHttp);

describe('Analytics Controller', () => {
    it('should get analytics data from Redis on /analytics/data GET', done => {
        chai.request(server)
            .get('/analytics/data?streamKey=testStream&start=-&end=+&count=10')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('messages');
                done();
            });
    });
});
