const Redis = require('redis');

const redisClient = Redis.createClient({
    host: 'localhost',
    port: 6379
});

redisClient.on('error', (err) => console.log('Redis Client Error', err));

exports.getAnalytics = async (req, res) => {
    try {
        const { streamKey, start, end, count } = req.query;
        const messages = await redisClient.xRange(streamKey, start, end, 'COUNT', count);
        res.status(200).send({ messages });
    } catch (error) {
        console.error('Error fetching data from stream:', error);
        res.status(500).send({ error: 'Internal server error' });
    }
};
