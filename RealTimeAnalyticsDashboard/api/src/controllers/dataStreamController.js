const Redis = require('redis');

const redisClient = Redis.createClient({
    host: 'localhost',
    port: 6379
});

redisClient.on('error', (err) => console.log('Redis Client Error', err));

exports.addToStream = async (req, res) => {
    try {
        const { streamKey, data } = req.body;
        const messageId = await redisClient.xAdd(streamKey, '*', ...Object.entries(data).flat());
        res.status(201).send({ message: 'Data added to stream', messageId });
    } catch (error) {
        console.error('Error adding data to stream:', error);
        res.status(500).send({ error: 'Internal server error' });
    }
};
