const redisClient = require('./redisClient');

class StreamModel {
    constructor(streamKey) {
        this.streamKey = streamKey;
    }

    async addToStream(data) {
        try {
            return await redisClient.xAdd(this.streamKey, '*', ...Object.entries(data).flat());
        } catch (error) {
            console.error('Error adding data to Redis Stream:', error);
            throw error;
        }
    }

    async getFromStream(start = '-', end = '+', count = 10) {
        try {
            return await redisClient.xRange(this.streamKey, start, end, 'COUNT', count);
        } catch (error) {
            console.error('Error retrieving data from Redis Stream:', error);
            throw error;
        }
    }
}

module.exports = StreamModel;
