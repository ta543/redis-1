const redisClient = require('./redisClient');

class GenericModel {
    static async setKey(key, value) {
        try {
            return await redisClient.set(key, value);
        } catch (error) {
            console.error('Error setting Redis key:', error);
            throw error;
        }
    }

    static async getKey(key) {
        try {
            return await redisClient.get(key);
        } catch (error) {
            console.error('Error getting Redis key:', error);
            throw error;
        }
    }

    static async deleteKey(key) {
        try {
            return await redisClient.del(key);
        } catch (error) {
            console.error('Error deleting Redis key:', error);
            throw error;
        }
    }
}

module.exports = GenericModel;
