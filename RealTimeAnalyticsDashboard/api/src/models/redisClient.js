const Redis = require('redis');

const redisClient = Redis.createClient({
    host: 'localhost',
    port: 6379,
    retry_strategy: options => {
        // Reconnect after a delay
        return Math.min(options.attempt * 100, 3000);
    }
});

redisClient.on('connect', () => console.log('Redis client connected'));
redisClient.on('error', (err) => console.error('Redis Client Error', err));

module.exports = redisClient;
