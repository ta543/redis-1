const express = require('express');
const router = express.Router();
const dataStreamController = require('../controllers/dataStreamController');

router.post('/add', dataStreamController.addToStream);

module.exports = router;
