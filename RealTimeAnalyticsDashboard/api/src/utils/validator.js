const validateStreamInput = (req, res, next) => {
    const { streamKey, data } = req.body;
    if (!streamKey || typeof data !== 'object') {
        return res.status(400).json({ error: 'Invalid input: Missing streamKey or data' });
    }
    next();
};

module.exports = {
    validateStreamInput
};
