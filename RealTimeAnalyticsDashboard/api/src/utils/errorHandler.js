const errorHandler = (err, req, res, next) => {
    if (res.headersSent) {
        return next(err);
    }
    console.error(err);  // Log error information for debugging
    res.status(500).json({ error: 'Internal server error' });
};

module.exports = errorHandler;
